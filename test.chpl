use Sort;
use BlockDist;
use Utility;
use SpAssign;
use Time;


config var nnz = 2;
config var size = 10;
config var debugPrint = 0;
config var nthreads = 1;
config var timing = 0;

proc applyTest()
{
  var spVec = createSpVecRandFast(size, nnz);
  var f = lambda(x:int) { return x + 1; };
  if(debugPrint!=0)
  {
    writeln("Before apply (+=1): ");
    writeln("domain: ", spVec.domain);
    writeln("Array: ", spVec);
  }

   var t1, t2 : Timer;
   t1.start();
   for i in 1..10 do
    Apply1(spVec, f);
   t1.stop();

   t2.start();
   for i in 1..10 do
    Apply2(spVec, f);
   t2.stop();
  
  if(debugPrint!=0)
  {
    writeln("After apply (+=1): ");
    writeln("domain: ", spVec.domain);
    writeln("Array: ", spVec);
  }

  writeln(" (locales, nthreads, apply1 and apply2): ", numLocales, " ", nthreads, " ", t1.elapsed()/10, " " ,t2.elapsed()/10);

}


proc assignTest()
{
  var spVec1 = createSpVecRandFast(size, nnz);
  var spVec2 = createSpVecRandFast(size, nnz);

  if(debugPrint!=0)
  {
    writeln("Before spAssign : ");
    writeln("domain1: ", spVec1.domain);
    writeln("Array1: ", spVec1);
    writeln("domain2: ", spVec2.domain);
    writeln("Array2: ", spVec2);

  }

   var t1, t2 : Timer;
   t1.start();
   spArrDistAssign1(spVec1, spVec2);  
   t1.stop();
   t2.start();
   spArrDistAssign2(spVec1, spVec2);  
   t2.stop();

  
  if(debugPrint!=0)
  {
    writeln("After spAssign : ");
    writeln("domain1: ", spVec1.domain);
    writeln("Array1: ", spVec1);
    writeln("domain2: ", spVec2.domain);
    writeln("Array2: ", spVec2);
  }

  writeln(" (capacity, nnzAfter, locales, threads, assign1 and assign2): ", size, " ", spVec2.size, " ", numLocales, " ",  nthreads, " ", t1.elapsed(), " " ,t2.elapsed());
}



proc eWiseMultTest()
{
  
  var tDenseCreate,tDenseInit,tSpCreate,t5 : Timer;
  if(timing!=0) then  tDenseCreate.start();
  var dDomDist = {0..#size} dmapped Block({0..#size}); 
  var dVec : [dDomDist] bool;
  if(timing!=0) 
  {
    tDenseCreate.stop();
    writeln("Dense vector create. size: ", size, " time: ", tDenseCreate.elapsed());
  }

  if(timing!=0) then  tDenseInit.start();
  var dnnz = nnz/2: int;
  initDenseVecFast(dVec, dnnz, true);
  if(timing!=0) 
  {
    tDenseInit.stop();
    writeln("Dense vector init. size: ", size, " time: ", tDenseInit.elapsed());
  }


  if(timing!=0) then  tSpCreate.start();
  var spVec = createSpVecRandFast(size, nnz);
  if(timing!=0) 
  {
    tSpCreate.stop();
    writeln("Sparse vector create. size: ", size, " nnz: ", spVec.domain.size, " time: ", tSpCreate.elapsed());
  }

  if(debugPrint!=0)
  {
    writeln("Before spAssign : ");
    writeln("sparse domain: ", spVec.domain);
    writeln("sparse Array: ", spVec);
    writeln("dense Array: ", dVec);
  }

   var t1 : Timer;
   t1.start();
   var spVec2 = eWiseMult(spVec, dVec, nthreads);  
   t1.stop();

  
  if(debugPrint!=0)
  {
    writeln("After spAssign : ");
    writeln("sparse domain: ", spVec2.domain);
    writeln("sparse Array: ", spVec2);
    writeln("dense Array: ", dVec);
  }

  writeln(" ( capacity, nnzBefore, nnzAfter, locales, threads, time): ",  size, " ", spVec.size, " " , spVec2.size, " ", numLocales, " ",  nthreads,  " ",t1.elapsed());
  //if(timing!=0) 


}



proc main()
{
  applyTest();
  //assignTest();
  //eWiseMultTest();
//writeln( (exp2(log2(1)+1.0)):int);
  //var dom = 1..1;
  //var dDomDist = {0..#size} dmapped Block({0..#size}); 
  //var dVec : [dDomDist] bool;

  //initDenseVecFast(dVec, 5, true);
  //writeln(dVec); 

   //var spVec = createSpVecRandFast(size, nnz);
  
  //writeln(spVec); 


}





