module Main
{

  use Generators;
  use BuildSpMat;
  use Utility;
  use MatrixMarket;
  use SpMSpV;
  use BlockDist;

  use BFS;

  //config const dataParTasksPerLocale = 2;
  config var scale = 9; // scale
  config var d = 4;
  config var f = .25; // fraction of full of the vector
  //config var nz = (n*f) : int;
  config const fname = "";
  //config type sparseLayoutType = DefaultDist; //COO
  config type sparseLayoutType = CSR; //COO



  proc main()
  {
    // read matrix market file  
        //var nz = (nrows * f) : int ;

       //var (nrows, ncols, inds, vals) = MatrixMarket.mmread(fname);
    //var A = BuildDistSpMatCSR(nrows, ncols, inds, vals);
    var n = 2**scale;
    var A = BuildDistSpMatER(n, d) ;

    //PrintDistCSR(A);
        
    BFSDist(A, n);

  




    /*
    var x = RandVec(nrows, nz); 

    var y2 = SpMSpV_SPA(A,nrows, x);
    //var y1 = SpMSpV(A, x);
    var y1 = SpMSpV(A, x);
    // verify correctness
    var eq = (y1==y2);
    var tt : bool;
    forall elm in eq with (& reduce tt) 
    {
      tt &= elm;
    }
    if(tt==true)  
    {
      writeln("The result vectors are equal");
    }
    else
    {
      writeln("The result vectors are not equal");
    }
*/
  }
}
