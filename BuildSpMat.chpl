module BuildSpMat
{
  use LayoutCSR;
  use Time;
  use BlockDist;
  use Random;
  use Sort;


  proc BuildSpMatCSR(nrows, ncols, inds, vals) 
  {
    var t: Timer;
    const denseDom = {0..nrows-1, 0..ncols-1};

    var spDom: sparse subdomain(denseDom) dmapped new dmap(new CSR());
    // Populate the sparse domain 
    t.start();
    spDom += inds; // much faster 
    /*
    for (row, col) in zip(rinds, cinds) do    
        spDom += (row,col); 
    */ 

    var A : [spDom] int; 
    for (ij, k) in zip(spDom, 0..) do
          A[ij] = vals[k] : int;

    t.stop();
    writeln("populating CSR sparse matrix took: ", t.elapsed()," seconds");
    stdout.flush();
    return A;
  }


    proc BuildDistSpMatER(n, d) 
  {
    var t: Timer;
        t.start();

    const denseDom = {0..#n, 0..#n} dmapped Block({0..#n, 0..#n}, sparseLayoutType=CSR);
    var spDom: sparse subdomain(denseDom);

    //var 
     var rs = new RandomStream(); // random stream 

     var l = 0;
     var inds: [0..#n*d] 2*int;
     for i in 0..n-1  // for each row .. test for each column as well
     {
        for k in 0..d-1 
        {
          var j = (rs.getNext() * (n-1)) : int ; 
          inds[l] = (i,j);
          l += 1;
        }
      }
    // Populate the sparse domain 
    //writeln(inds);
    spDom += inds; // much faster 
   //writeln(spDom);

    
    var A : [spDom] int; 
  
    t.stop();
    writeln("populating CSR sparse matrix took: ", t.elapsed()," seconds");
    stdout.flush();
    return A;
  }
 

  proc BuildDistSpMatER_fast(n, d) 
  {
    var t: Timer;
        t.start();

    const denseDom = {0..#n, 0..#n} dmapped Block({0..#n, 0..#n}, sparseLayoutType=CSR);
    var spDom: sparse subdomain(denseDom);

    var ldoms =   spDom._value.locDoms;
    coforall ldom in ldoms do
      on ldom
      {
        ref locMatDom =  ldom.mySparseBlock; 
        var riLow = ldom.mySparseBlock.dim(1).low;
        var riHigh = ldom.mySparseBlock.dim(1).high;
        var ciLow = ldom.mySparseBlock.dim(2).low;
        var ciHigh = ldom.mySparseBlock.dim(2).high; 
 
        var lnnz = n*d/numLocales: int; 
        lnnz += 1;
        var inds: [0..#lnnz] 2*int;
        var actualNnz = 0;
        var rs = new RandomStream(); // random stream 

        for i in riLow..riHigh // check if I need -1
        {
          for k in 0..#d 
          {
            // generate a column id
            var j = (rs.getNext() * (n-1)) : int ; 
            // check if it belongs to me
            if(j>=ciLow && j <=ciHigh)
            {
              inds[actualNnz] = (i,j);
              actualNnz  += 1;
            }
          }
        }
        inds.remove(actualNnz, lnnz-actualNnz); 
        ldom.mySparseBlock += inds; 
      } 
       
    spDom._value.nnz = getGlobalNnz(spDom);
    var A : [spDom] int; 
  
    t.stop();
    //writeln("populating CSR sparse matrix took: ", t.elapsed()," seconds");
    stdout.flush();
    return A;
  }
 
 
  proc BuildDistSpMatCSR(nrows, ncols, inds, vals) 
  {
    writeln(inds);
    var t: Timer;
    const denseDom = {0..#nrows, 0..#ncols} dmapped Block({0..#nrows, 0..#ncols}, sparseLayoutType=CSR);
    var spDom: sparse subdomain(denseDom);

    // Populate the sparse domain 
    t.start();
    spDom += inds; // much faster 
    /*
    for (row, col) in zip(rinds, cinds) do    
        spDom += (row,col); 
    */ 

    
    var A : [spDom] int; 
    for (ij, k) in zip(spDom, 0..) do
          A[ij] = vals[k] : int;

    t.stop();
    writeln("populating CSR sparse matrix took: ", t.elapsed()," seconds");
    stdout.flush();
    return A;
  }

  
  // TODO: should be reaplced by bulkAdd
  proc BuildSpMatCOO(nrows, ncols, inds, vals) 
  {
    var t: Timer;
    const denseDom = {0..#nrows, 0..#ncols};

    var spDom: sparse subdomain(denseDom);
    // Populate the sparse domain 
    t.start();
    spDom += inds; 
    var A : [spDom] int; 
    for (ij, k) in zip(A.domain, 0..) do
      A[ij] = vals[k] : int;

    t.stop();
    writeln("populating COO sparse matrix took: ", t.elapsed()," seconds");
    return A;
  }



  proc BuildSpMat(nrows, ncols, inds, vals, sparseLayoutType) 
  {
    var t: Timer;
    const denseDom = {0..#nrows, 0..#ncols} dmapped Block({0..#nrows, 0..#ncols},  sparseLayoutType=sparseLayoutType);
    var spDom: sparse subdomain(denseDom);
    // Populate the sparse domain 
    t.start();
    spDom += inds; 
    var A : [spDom] int; 
    for (ij, k) in zip(A.domain, 0..) do
      A[ij] = vals[k] : int;

    t.stop();
    writeln("populating sparse matrix took: ", t.elapsed()," seconds");
    return A;
  }

   proc BuildSpDom(nrows, ncols, type sparseLayoutType) 
  {
    const denseDom = {0..#nrows, 0..#ncols} dmapped Block({0..#nrows, 0..#ncols},  sparseLayoutType=sparseLayoutType);
    var spDom: sparse subdomain(denseDom);
    return spDom;
  }

  proc populateSpArr(ref arr, ref dom, inds, vals)
  {
    var t: Timer;
    t.start();
    dom += inds;
    for (a,v) in zip(arr, vals) do a = v : int;
    t.stop();
    writeln("populating sparse matrix took: ", t.elapsed()," seconds");

  }


}
