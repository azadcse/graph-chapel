module spmspvtest
{
use Random;
use  BuildSpMat;
use Utility;
use SpMSpV;


config const n = 4;
config const d = 4;
config const f = .25;

 proc main()
  {

var nnzx = (n*f): int;
//writeln(nnzx, "****"); 
var A = BuildDistSpMatER_fast(n, d);

//PrintDistCSR(A);
//PrintDistSparse(A);

var x = createSpVecRandFast(n, nnzx);
//writeln("*********** x: ", x.domain.size);

var y = SpMSpV_SPA_BFS_dist(A, n, x);
//writeln("*********** y: ", y.domain.size);
writeln(" xnnz: ", x.domain.size, " ynnz: ", y.domain.size);
//writeln("*********** x: ", x.domain);
//writeln("*********** y: ", y.domain);
  }

}





