module SpMSpV
{
use LayoutCSR;
use Time;
use BlockDist;
use Random;
use Sort;

  config const nthreads = 1;
  config const timing = true;


// Naive version. Using blukAdd made it fast.
// It is never used
proc SpMSpV(A: [?MatDom], X: [?VectDom]) 
{
   var t: Timer;
   t.start();

  var YDom: VectDom.type;
  var Y: [YDom] int;
  for (rid,inval) in zip(VectDom,X)
  {
    var rstart = MatDom._value.rowStart(rid);
    var rend = MatDom._value.rowStop(rid);
    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var id = MatDom._value.colIdx(nzidx);
        var val =  inval * A._value.data(nzidx);
        YDom += id;
        Y[id] += val;
      }
    }
  }
  t.stop();
  writeln(" Naive SpMSpV took: ", t.elapsed()," seconds");
  stdout.flush();
  return Y;
}





// SpMV with SPA
proc SpMSpV_SPA(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();

  var isthere : [0..n-1] bool;
  var localy : [0..n-1] int;
  var nzinds : [0..n-1] int;
  var k = 0;

  t1.start();
  for (rid,inval) in zip(VectDom,X)
  {
    var rstart = MatDom._value.rowStart(rid);
    var rend = MatDom._value.rowStop(rid);
    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var colid = MatDom._value.colIdx(nzidx);
        var val =  inval * A._value.data(nzidx);
        if(!isthere[colid])
        {
          localy[colid] = val;
          nzinds[k] = colid;
          k+=1;
          isthere[colid] = true;
        }
        else
        {
          localy[colid] = localy[colid] + val;
        }
      }
    }
  }

  t1.stop();
  t2.start();
  // resize the resultant domain to remove unused entries 
  nzinds.remove(k, n-k); 
  quickSort(nzinds); // replace with radix sort if avaiable 
  t2.stop();
  t3.start();

  var YDom: VectDom.type;
  //YDom = nzinds;
  YDom.bulkAdd(nzinds,true, true, false); // sorted and no duplicates
  
  var Y: [YDom] int;  
  for (i,j) in zip(YDom,0..) // forall does not work   
  {
     Y[i] = localy[nzinds[j]];

  }

  t3.stop();
  t.stop();
  writeln(" SPA-based SpMSpV took: ", t.elapsed()," seconds");
  writeln(" t1: ", t1.elapsed(), " t2: ",t2.elapsed()," t3: ", t3.elapsed());
  stdout.flush();
  return Y;
}






// For BFS
// (select2nd, min) semiring
proc SpMSpV_SPA_BFS(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();

  var isthere : [0..n-1] bool;
  var localy : [0..n-1] int;
  var nzinds : [0..n-1] int;
  var k = 0;

  t1.start();
  for (rid,inval) in zip(VectDom,X)
  {
    var rstart = MatDom._value.rowStart(rid);
    var rend = MatDom._value.rowStop(rid);
    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var colid = MatDom._value.colIdx(nzidx);
        var val =  inval * A._value.data(nzidx);
        if(!isthere[colid])
        {
          nzinds[k] = colid;
          k+=1;
          isthere[colid] = true;
          localy[colid] = rid;
        }
      }
    }
  }

  t1.stop();
  t2.start();
  // resize the resultant domain to remove unused entries 
  nzinds.remove(k, n-k); 
  quickSort(nzinds); // replace with radix sort if avaiable 
  t2.stop();
  t3.start();

  var YDom: VectDom.type;
  //YDom = nzinds;
  YDom.bulkAdd(nzinds,true, true, false); // sorted and no duplicates 
  var Y: [YDom] int; 
  for (i,j) in zip(YDom,0..) // forall does not work   
  {
     Y[i] = localy[nzinds[j]];
  }
 
 
  t3.stop();
  t.stop();
  writeln(" SPA-based SpMSpV for BFS took: ", t.elapsed()," seconds");
  writeln(" t1: ", t1.elapsed(), " t2: ",t2.elapsed()," t3: ", t3.elapsed());
  stdout.flush();
  return Y;
}


proc SpMSpV_SPA_ColSplit(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();

  var isthere : [0..n-1] atomic bool;
  var localy : [0..n-1] int;
  var nzinds : [0..n-1] int;
  var k : atomic int;
  k.write(0);

  t1.start();
  forall (rid,inval) in zip(VectDom,X)
  {
    var rstart = MatDom._value.rowStart(rid);
    var rend = MatDom._value.rowStop(rid);
    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var colid = MatDom._value.colIdx(nzidx);
        var val =  inval * A._value.data(nzidx);
        if(!isthere[colid].read())
        {
          nzinds[ k.fetchAdd(1)] = colid;
          isthere[colid].write(true);
          localy[colid] = rid;
        }
      }
    }
  }

  t1.stop();
  t2.start();
  // resize the resultant domain to remove unused entries 
  nzinds.remove(k.read(), n-k.read()); 
  // parallel merge sort
  // only merge sort is parallel at this moment
  mergeSort(nzinds); // replace with radix sort if avaiable 
  t2.stop();
  t3.start();

  var YDom: VectDom.type;
  //YDom = nzinds;
  YDom.bulkAdd(nzinds,true, true, false); // sorted and no duplicates 
  var Y: [YDom] int; 
  forall (i,j) in zip(YDom,0..) // forall does not work   
  {
     Y[i] = localy[nzinds[j]];
  }
 
 
  t3.stop();
  t.stop();
  writeln(" SPA-based SpMSpV for BFS took: ", t.elapsed()," seconds");
  writeln(" t1: ", t1.elapsed(), " t2: ",t2.elapsed()," t3: ", t3.elapsed());
  stdout.flush();
  return Y;
}



// used in distributed memory code
proc SpMSpV_SPA_1(A: [?MatDom], X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();
 
  var ciLow = MatDom.dim(2).low;
  var ciHigh = MatDom.dim(2).high; 
  var ncol = MatDom.dim(2).size;  
  var isthere : [ciLow..ciHigh] atomic bool;
  var localy : [ciLow..ciHigh] int;
  var nzinds : [0..#ncol] int;
  var k : atomic int;
  k.write(0);

   if(timing) then t1.start();
  forall (rid,inval) in zip(VectDom,X)
  {
    var rstart = MatDom._value.rowStart(rid);
    var rend = MatDom._value.rowStop(rid);
    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var colid = MatDom._value.colIdx(nzidx);
        var val =  inval * A._value.data(nzidx);
        if(!isthere[colid].read())
        {
          nzinds[ k.fetchAdd(1)] = colid;
          isthere[colid].write(true);
          localy[colid] = rid;
        }
      }
    }
  }

   if(timing) then t1.stop();
   if(timing) then t2.start();
  // resize the resultant domain to remove unused entries 
  nzinds.remove(k.read(), ncol-k.read()); 
  // parallel merge sort
  // only merge sort is parallel at this moment
  mergeSort(nzinds); // replace with radix sort if avaiable 
   if(timing) then t2.stop();
   if(timing) then t3.start();

  
  
  const yParentDom = {ciLow..ciHigh};
  var yDom: sparse subdomain(yParentDom);
 
    //writeln(A.domain);
  
    // distributed fringe
    

  //var YDom: VectDom.type;
  //YDom = nzinds;
  //yDom.bulkAdd(nzinds,true, true, false); // sorted and no duplicates 

  var Y: [yDom] int; 
  bulkIndCopy(Y, nzinds);

  forall (si,sv) in zip(yDom, Y)
  {
      sv = localy[si];
  }

  /*
  for (i,j) in zip(yDom,0..)   // foall stopped working now ??
  {
     Y[i] = localy[nzinds[j]];
  }
 */
 
   if(timing) then t3.stop();
  t.stop();
  //writeln(" SPA-based SpMSpV for BFS took: ", t.elapsed()," seconds");
   if(timing && here.id==0) then 
     write(" ", t1.elapsed(), "  ",t2.elapsed()," ", t3.elapsed(), " ");
  //stdout.flush();
  return Y;
}



proc bulkIndCopy( sv: [?spDom], dVec: [?dDom])
{
    spDom._value.nnz = dDom.size;
    spDom._value.nnzDom = {1..dDom.size};
    //var spLow = spDom.low;
    //var spHigh = spDom.high;
    forall (si,dv) in zip(1..dDom.size, dVec)
    {
      spDom._value.indices[si] = dv;
    }
}




// currently it serves the purpose of removing visited vertices 
//TODO: need to make it general eWiseMult / setminus

  proc denseToSparse( visited: [?vecDom], numTasks)
  {

    var lo = vecDom.low; 
    var hi = vecDom.high;
    const denseDom = {lo..hi} dmapped Block({lo..hi});
    var newDom: sparse subdomain(denseDom);
    var newArr: [newDom]  int; // change this

    var newDoms =  newDom._value.locDoms;
    var newArrs =  newArr._value.locArr;
    var dArrs = visited._value.locArr;
    //const numTasks = 1; // check how to determine this 
    coforall (newDom,newArr, dArr) in zip(newDoms, newArrs, dArrs) do
      on newDom 
      {
        var localLow =  dArr.myElems.domain.low; 
        //var localHigh =  dArr.locDom.high; 
        var oldnnz = dArr.myElems.size;
        var perthread = (oldnnz / numTasks) : int;

        var keepInd : [localLow..#oldnnz] newDom.idxType;
        //var keepVal : [0..#oldnnz] newArr.eltType;
        //var lo = oldArr.myElems.domain._value.nnzDom.low; // usually 1
        var tStartInds : [0..#numTasks] int;
        var tEndInds : [0..#numTasks] int;
        var tDisp : [0..numTasks] int; // size = numTasks+1 
       // writeln(here.id, " # dArr.domain: ", dArr.locDom, " perthread", perthread, "  ", oldnnz);
        stdout.flush();
        coforall tid in 0..#numTasks // shared memory parallel
        {
          var tStartInd = localLow + perthread * tid;
          var tEndInd = localLow + perthread * (tid+1);
          if(tid == numTasks) then
            tEndInd = localLow + oldnnz; 
          var k = tStartInd;
          for i in  tStartInd..(tEndInd-1)
          {
            //var spInd =  oldDom.mySparseBlock._value.indices[i+lo];  
            //var spVal = oldArr.myElems._value.data[i+lo]; 
            //if(!dArr[i])
            if(dArr.myElems[i].read())  
            {
                keepInd[k] = i;
                //keepVal[k] = spVal;
                k += 1;
                //dArr[spInd] = true;
            }  
          }
          tStartInds[tid] = tStartInd;
          tEndInds[tid] = k;
        } 

        // compute displacement 
        tDisp[0] = 0;
        for i in 0..#numTasks
        {
          tDisp[i+1] = tDisp[i] +  tEndInds[i] -  tStartInds[i];
        }

        var newLocalNnz = tDisp[numTasks];

        //newDom.mySparseBlock._value._bulkGrow(newLocalNnz);
        newDom.mySparseBlock._value.nnz = newLocalNnz;
        newDom.mySparseBlock._value.nnzDom = {1..newLocalNnz};

        coforall tid in 0..#numTasks // shared memory parallel
        {
          var tStartInd1 = tStartInds[tid];
          var tEndInd1 =  tEndInds[tid];
          var tStartInd2 = tDisp[tid];
          var tEndInd2 = tDisp[tid+1];

          for (i,j) in  zip(tStartInd1..(tEndInd1-1), tStartInd2..(tEndInd2-1))
          {
            newDom.mySparseBlock._value.indices[j+1] = keepInd[i];  
            //newArr.myElems._value.data[j+lo] = keepVal[i]; 
          }
        }  
  }
  newDom._value.nnz = getGlobalNnz(newDom);
  return newArr;

  }



proc SpMSpV_SPA_BFS_dist(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t7: Timer;
    var el1, el2, el3, el4, el5, el6, el7: real;
   t.start();

  var distM = MatDom._value.dist;
  var distV = VectDom._value.dist;

  var pc = MatDom._value.dist.targetLocDom.dim(2).size;
  //var isthere : [low..high] atomic bool;
  //const  isthere = {0..#n, 0..#pc} bool dmapped Block({0..#n, 0..#pc});

  //const domDY = {0..#n, 0..#pc} dmapped Block({0..#n, 0..#pc});
  //var domY: sparse subdomain(domDY);


    const denseDom = {0..#n} dmapped Block({0..#n});
   //var yDom: sparse subdomain(denseDom);
   //var y: [yDom] int;
   if(timing) then t1.start();
   var isthere: [denseDom]  atomic bool;
   var localy: [denseDom]  int;
   var nzinds:  [denseDom]  int;
  // parallel across locales
  if(timing) then t1.stop();

  coforall l in  distM.targetLocDom do on distM.targetLocales[l]   
  {
      //writeln(here.id, "****");

       var t2, t3, t4, t5, t6: Timer;

      //var locMatDom = MatDom.localSubdomain();
      ref locMatDom =  MatDom._value.locDoms[l].mySparseBlock; 
      ref locArr =  A._value.locArr[l].myElems; 
      
      var lr = locMatDom.dim(2).low;
      var hr = locMatDom.dim(2).high; 
      //var isthere : [lr..hr] bool;
      //var localy : [lr..hr] int;
      //var nzinds : [lr..hr] int;
      
      // number of locales in the grid (0=based indexing in grid)
      var pcol = MatDom._value.dist.targetLocDom.dim(2).size;
      var prow = MatDom._value.dist.targetLocDom.dim(1).size;
    
      if(timing) then t3.start();

      
      // local x
       var localXDom: sparse subdomain({locMatDom.dim(1).low..locMatDom.dim(1).high});  

     /*  
        for i in  distM.targetLocDom.dim(2) 
      {
          var remoteVectDom =  VectDom._value.locDoms[(l(1) * pcol +  i)].mySparseBlock;
          localXDom.bulkAdd(remoteVectDom._value.indices(1..remoteVectDom.size),true, true, false);        
      }  
    */

      // compute size of the replicated vector     
       var rnnz = 0;
      for i in  distM.targetLocDom.dim(2) 
      {
          var remoteVectDom =  VectDom._value.locDoms[(l(1) * pcol +  i)].mySparseBlock;
          rnnz += remoteVectDom.size;    
      }  

      localXDom._value.nnz = rnnz;
      localXDom._value.nnzDom = {1..rnnz};
    

      // copy replicated indices from rimote locations 
      var rxi = 1; 
      // access remote parts of x along the processor row
      for i in  distM.targetLocDom.dim(2) 
      {
          var remoteVectDom =  VectDom._value.locDoms[(l(1) * pcol +  i)].mySparseBlock;
          forall (si,di) in zip(remoteVectDom._value.indices(1..remoteVectDom.size), rxi..#remoteVectDom.size) do
            localXDom._value.indices[di] = si; 
          rxi += remoteVectDom.size;                 
      }  

      var localX: [localXDom] int; 

       if(timing) then t3.stop();
      //if(timing) then t4.start();

      /*
      forall ind in localXDom  // ignoring now for BFS
      {
        localX[ind] = X[ind];
        // not sure if it would be faster to use the folowing
        // var locVec = X._value.locArr[(l(1) * pcol +  i)].myLocArr;

      }
       */

        // if(timing) then t4.stop();
         if(timing) then t5.start();

     // writeln("required x on locale ", l, " : ", localX.domain);
      //stdout.flush(); 
     var localY = SpMSpV_SPA_1(locArr, localX);
     //writeln("replicated y on locale ", l, " : ", localY.domain);
     //stdout.flush();
     //yDom += localY.domain; // no it will not work!!
    //writeln("replicated y on locale ", l, " : ", localY.domain);
     //stdout.flush();
      if(timing) then  t5.stop();
      if(timing) then t6.start();

     forall (id,inval) in zip(localY.domain,localY)
      {
        if(!isthere[id].read()) // works only for BFS
        {
          //nzinds[ k.fetchAdd(1)] = id;
          isthere[id].write(true);
          //localy[id] = rid;
          //y[id] = inval; // it will be very slow
        }
      }
      if(timing) then t6.stop();
      if(timing && here.id==0) then 
          write(" init: ", t1.elapsed(), " gatherx: ", t3.elapsed(), " localmult: ", t5.elapsed(), " scattery: ", t6.elapsed());


  }

   if(timing) then t7.start();

   var y = denseToSparse(isthere, nthreads);
    //  PrintDistSparse(y);

   if(timing) then t7.stop();
   if(timing) then  write(" output: ", t7.elapsed());


  t.stop();
  //writeln(" SPA-based SpMSpV for BFS took....: ", t.elapsed()," seconds");
  stdout.flush();
  return y;
   
}











// value is ignored
// (select2nd, min) semiring
proc SpMSpV_SPA_BFS_COO(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();

  var isthere : [0..n-1] bool;
  var localy : [0..n-1] int;
  var nzinds : [0..n-1] int;
  var k = 0;

  t1.start();
  for (rid,inval) in zip(VectDom,X)
  {
    var rstart = BinarySearch(MatDom._value.indices, (rid,-1))[2];
    var rend = BinarySearch(MatDom._value.indices, (rid+1,-1))[2];
    rstart = rstart - 1; // 0-based indexing
    rend = rend - 1; // 0-based indexing
    //if(rstart >= MatDom.size ) rstart = MatDom.size;

    if(rend>=rstart)
    {
      for nzidx in rstart..rend // for each nonzero of the selected row
      {
        var (rowid, colid) = MatDom._value.indices(nzidx);
        var val =  inval * A._value.data(nzidx);
        if(!isthere[colid])
        {
          nzinds[k] = colid;
          k+=1;
          isthere[colid] = true;
          localy[colid] = rid;
        }
      }
    }
  }

  t1.stop();
  t2.start();
  // resize the resultant domain to remove unused entries 
  nzinds.remove(k, n-k); 
  quickSort(nzinds); // replace with radix sort if avaiable 
  t2.stop();
  t3.start();

  var YDom: VectDom.type;
  //YDom = nzinds;
  YDom.bulkAdd(nzinds,true, true, false); // sorted and no duplicates 
  var Y: [YDom] int; 
  for (i,j) in zip(YDom,0..) // forall does not work   
  {
     Y[i] = localy[nzinds[j]];
  }
 
 
  t3.stop();
  t.stop();
  writeln(" SPA-based SpMSpV for BFS took: ", t.elapsed()," seconds");
  //writeln(" t1: ", t1.elapsed(), " t2: ",t2.elapsed()," t3: ", t3.elapsed());
  stdout.flush();
  return Y;
}


proc SpMSpV_SPA_BFS_COO_dist(A: [?MatDom], n, X: [?VectDom]) 
{
   var t, t1, t2, t3: Timer;
   t.start();

  var isthere : [0..#n] bool;
  var localy : [0..#n] int;
  var nzinds : [0..#n] int;
  var distM = MatDom._value.dist;
  var distV = VectDom._value.dist;


  // parallel across locales

  coforall l in  distM.targetLocDom do on distM.targetLocales[l]   
  {
      var locMatDom =  MatDom._value.locDoms[l].mySparseBlock; // local domain
     // var locArr =  A.domain._value.locDoms[l].mySparseBlock; // local array
      var lr = locMatDom.dim(2).low;
      var hr = locMatDom.dim(2).high; 
      var isthere : [lr..hr] bool;
      var localy : [lr..hr] int;
      var nzinds : [lr..hr] int;
      var k = lr;

      for i in  distM.targetLocDom.dim(2) 
      {
         var pcol = VectDom._value.dist.targetLocDom.dim(2).high;
         var locVectDom =  VectDom._value.locDoms[(l(1) * pcol +  i)].mySparseBlock;     
         var locVec = X._value.locArr[(l(1) * pcol +  i)].myLocArr;
        for (rid,inval) in zip(locVectDom,locVec)
        {
          var rstart = BinarySearch(locMatDom._value.indices, (rid,-1))[2];
          var rend = BinarySearch(locMatDom._value.indices, (rid+1,-1))[2];
          rstart = rstart - 1; // 0-based indexing
          rend = rend - 1; // 0-based indexing


          if(rend>=rstart)
          {
            for nzidx in rstart..rend // for each nonzero of the selected row
            {
              var (rowid, colid) = MatDom._value.indices(nzidx);
              var val =  inval * A._value.data(nzidx);
              if(!isthere[colid])
              {
                nzinds[k] = colid;
                k+=1;
                isthere[colid] = true;
                localy[colid] = rid;
              }
            }
          }


          nzinds.remove(k, hr-lr-k); 
          quickSort(nzinds); // replace with radix sort if avaiable 
          t2.stop();
        }   
         

      }
    
  }
 
    t3.start();

   var YDom: VectDom.type;
   YDom += nzinds; 
   var Y: [YDom] int; 
   forall (i,j) in zip(YDom,0..) // forall does not work   
   {
        Y[i] = localy[nzinds[j]];
   }
 



  t3.stop();
  t.stop();
  writeln(" SPA-based SpMSpV for BFS took: ", t.elapsed()," seconds");
  //writeln(" t1: ", t1.elapsed(), " t2: ",t2.elapsed()," t3: ", t3.elapsed());
  stdout.flush();
  return Y;
   
}


}



