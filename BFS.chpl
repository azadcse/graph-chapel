module BFS
{
  use SpMSpV;
  use SpAssign;
  use Time;
  config var debugPrint = 0;
  config var nthreads = 1;



  proc removeVisited(x: [?VectDom], visited)
  {
    var unvisited : [0..#x.size] int; //temporary dense array 
    var k = 0;
    for v in x.domain
    {
      if(!visited[v])
      {
        visited[v] = true;
        unvisited[k] = v;
        k = k + 1;
      }  
    }
    unvisited.remove(k, x.size-k);
    VectDom.clear();

    if(k>0)
    {
      VectDom.bulkAdd(unvisited,true, true, false); // sorted and no duplicates
      for i in VectDom  
      {
        x[i] = i;
      }
    }
  }
 

    /*
   proc removeVisitedDistBulk(x: [?vecDom], visited)
  {

    var lo = vecDom.low; 
    var hi = vecDom.high;
    const denseDom = {lo..hi} dmapped Block({lo..hi});
    var newDom: sparse subdomain(denseDom);
    var newArr: [newDom] x.eltType;

    var oldDoms =  vecDom._value.locDoms;
    var newDoms =  newDom._value.locDoms;
    var oldArrs =  x._value.locArr;
    var newArrs =  newArr._value.locArr;
    var dArrs = visited._value.locArr;
    coforall (oldDom,newDom,oldArr, newArr, dArr) in zip(oldDoms, newDoms, oldArrs, newArrs, dArrs) do
      on oldDom 
      {
       //local // this is not entirely local!! why??
       {
         var oldnnz = oldDom.mySparseBlock.size;
          var keepInd : [0..#oldnnz] oldDom.idxType;
          var keepVal : [0..#oldnnz] oldArr.eltType;

          var k=0;
          for (ind, val) in zip(oldDom.mySparseBlock, oldArr.myElems) 
          {
            if(!dArr[ind])
            {
              keepInd[k] = ind;
              keepVal[k] = val;
              k+=1;
              dArr[ind] = true;
            }
          }
          
          if(k>0)
          {
            keepInd.remove(k, oldnnz-k); 
            newDom.mySparseBlock.bulkAdd(keepInd,true, true, false); 
            //copy value
            k = 0;
            for newval in newArr.myElems
            {
              newval = keepVal[k];
              k += 1;
            }
          }
        }
      }
  

    newDom._value.nnz = getGlobalNnz(newDom);
    return newArr;
  }
*/

    // carefule, this only work for 1D at this moment
  // TODO: think if this can be made general 
   proc removeVisitedDist(x: [?VectDom], visited)
  {

    var lo = VectDom.low; 
    var hi = VectDom.high;
    const denseDom = {lo..hi} dmapped Block({lo..hi});
    var spDom: sparse subdomain(denseDom);
    //var unvisited: [spDom] int;   
    for v in x.domain
    {
      if(!visited[v]) // should not communicate 
      {
        visited[v] = true; // should not communicate 
        spDom += v;
        //writeln(v, ".....");
        //unvisited[v] = x[v]; // should not communicate, but may be expensive because needs to resize unvisited
      }  
    }
    writeln(spDom, ".....");

    writeln("done adding");
    var unvisited: [spDom] int; 
    return unvisited;
  }



  proc BFS(A: [?MatDom], nv)
  {
    var tBFS: Timer;
    tBFS.start();

    const parentVecDom = {0..#nv};
    var visited : [0..#nv] bool;
    //writeln(A.domain);
    visited = false;
    var source = 1;
    var fDom: sparse subdomain(parentVecDom);
    fDom += source;
    var f: [fDom] int;
    f[source]=1;
    visited[source] = true;
    var it = 1;

    while(!f.isEmpty())
    {
      // writeln(f, "......");
      // for distributed COO
      //var f1 = SpMSpV_SPA_BFS_COO_dist(A, nv, f);
      //var f1 = SpMSpV_SPA_BFS(A, nv, f);
      writeln ("Iteration: ", it);
      it += 1;
      //var f1 = SpMSpV_SPA_ColSplit(A, nv, f);
      var f1 = SpMSpV_SPA_BFS_dist(A, nv, f);


      //var f1 = SpMSpV(A, f);

      removeVisited(f1, visited);
      assign(f,f1);
    }
    tBFS.stop();
    writeln("\n-------------------------------------");
    writeln ("Time taken for BFS: ", tBFS.elapsed());
    writeln("-------------------------------------");

  }





  proc BFSDist(A: [?MatDom], nv)
  {
    var tBFS: Timer;
    tBFS.start();
    var source = 1;

    const denseDom = {0..#nv} dmapped Block({0..#nv});
    var visited : [denseDom] bool;
    visited = false;

    //writeln(A.domain);
  
    // distributed fringe
    var fDom: sparse subdomain(denseDom);
    fDom += source;
    var f: [fDom] int;
    f[source]=1;

    visited[source] = true;
    var it = 1;
    while(!f.isEmpty())
    //while(it<4)
    {

      writeln ("Iteration: ", it);
      writeln("======current frontier=========");
      if(debugPrint!=0) then
        PrintDistSparse(f);
      it += 1;
      var f1 = SpMSpV_SPA_BFS_dist(A, nv, f);

       stdout.flush();

      if(debugPrint!=0) then
        writeln("*********** f1: ", f1.domain);
       stdout.flush();

      var f2 = eWiseMult(f1, visited, nthreads);
      
       if(debugPrint!=0) then
        writeln("*********** f2: ", f2.domain);
        stdout.flush();
        spArrDistAssign2(f,f2);
        if(debugPrint!=0) then
          writeln("*********** f: ", f.domain);


    }
    tBFS.stop();
    writeln("\n-------------------------------------");
    writeln ("Time taken for BFS: ", tBFS.elapsed());
    writeln("-------------------------------------");

  }

}
