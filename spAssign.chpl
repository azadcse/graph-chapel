
module SpAssign
{
  use BlockDist;
  use Time;

  proc Apply1(spArr, unaryFuncion)
  {
    forall a in spArr do 
      a = unaryFuncion(a);
  }

  
  proc Apply2(spArr, unaryFuncion)
  {
      var locArrs =  spArr._value.locArr;     
      coforall locArr in locArrs do 
      on locArr 
      {
       local 
       {
         forall a in locArr.myElems do 
           a = unaryFuncion(a);
       }
      }
  }



proc getGlobalNnz(spDom)
{
  var locDoms =  spDom._value.locDoms;
  var gnnz = 0; 
  // there is no parallelization benefit here
  for locDom in locDoms do  
  {
      gnnz +=  locDom.mySparseBlock.size;  
  }  
      
  return gnnz;
}

proc spDomClear(spDom)
{
  spDom.clear();
  spDom._value.nnz = 0; // bug in Chapel, fixed by Brad
}

proc spAssignCheck(spDom1, spDom2)
{
  if(spDom1.dist != spDom2.dist) // they are equal if their bounding boxes and target locale sets are equal
  {
      warning(" NO-OP: CA-Assignment of block-distributed sparse domains is not allowed when their distributions are not equivalent.\n If you really want this use spDomDistAssign2."); 
     return false; 
  }
  else if(spDom1.dims() != spDom2.dims())
  {
    warning(" NO-OP: CA-Assignment of block-distributed sparse domains is not allowed when their parent domains are not equal."); 
    return false;
  }
  else return true;
 
  /*
  var locDoms1 =  spDom1._value.locDoms; 
  var locDoms2 =  spDom2._value.locDoms;
  var domComply = true;  
  for (locDom1,locDom2) in zip(locDoms1, locDoms2) do  //TODO: make coforall with a synch bool
      on locDom1 do
      {
          if(locDom1.parentDom != locDom2.parentDom)
          {
              domComply=false;
          }
       }
  if(!domComply)
  {
   warning(" NO-OP: CA-Assignment of block-distributed sparse domains is not allowed when parent domains of all local domains do not match."); 
     return false; 

  }
  else
    return true;
   */
 
}

/****************************************************************
// the usual one to be implemeted in Chapel as Michael promised
// starts in  proc =(ref a: domain, b: domain) function in ChapelArray.chpl
// This will be super slow because it removes element by element from  spDom1 
// and then adds element by element in  spDom2
// currently dsiIndsIterSafeForRemoving() is not defined for distributed sparse doamin
 ***************************************************************/
proc spDomDistAssign1(spDom1, spDom2)
{
  //spDom1 = spDom2;
  writeln("The default assignment of distributed sparse domains is not supported yet!");
}


/****************************************************************
 simplest implementation with clear() and += 
 += function is defined in  
      proc +=(ref sd: domain, d: domain) in ChapelDistributions.chpl 
 Performance concern: the above function gathers all indices of spDom2
  into a single locale (communication) and then call bulkAdd_help.
  the latter function (defined in bulkAdd_help in SparseBlockDist.chpl)
  scatters the gather data again to target locale and then add locally!
  Hence two rounds of unnecessary communication happens!
  We avoided this communication in spDomAssignDist3
 ***************************************************************/
proc spDomDistAssign2(spDom1, spDom2)
{
  spDomClear(spDom1);
  spDom1 += spDom2;
}

// default version 
/****************************************************************
 implementation without communicating data
 Performance concern: the local += function is not parallel. 
 It also can be optimized if the left hand side is an empty domain
 ***************************************************************/
proc spDomDistAssign3(spDom1, spDom2)
{


  if(!spAssignCheck(spDom1, spDom2) )
  {
    //warning(" NO-OP: CA-Assignment of block-distributed sparse domains is not allowed"); 
     return; 
  }
  
  spDomClear(spDom1);
  if(spDom2.size == 0) then
    return;

  var locDoms1 =  spDom1._value.locDoms; // array of local domain classess. No copy is made.
  var locDoms2 =  spDom2._value.locDoms;
    
  coforall (locDom1,locDom2) in zip(locDoms1, locDoms2) do //locDom1,locDom2 are references
      on locDom1 do
      {
          var nnz = locDom2.mySparseBlock.size;
          // growing nnzDom will grow both domain and array 
          //locDom1.mySparseBlock._value._bulkGrow(nnz);
          locDom1.mySparseBlock._value.nnz = nnz;
          locDom1.mySparseBlock._value.nnzDom = {1..nnz};


          // now locDom1,locDom2 have the same nnzDom
          // !!! careful: there is a danger with this nnzDom.size != nnz
          // (!!!! do not use this ) forall i in  locDom1.mySparseBlock._value.nnzDom do
          var lo = locDom1.mySparseBlock._value.nnzDom.low; // usually 1
           forall i in lo..#nnz do
            locDom1.mySparseBlock._value.indices[i] = locDom2.mySparseBlock._value.indices[i]; 
      }
  spDom1._value.nnz = spDom2.size;
}



/****************************************************************
 Another implementation without communicating data
 Performance concern: the local += function is not parallel. 
 It also can be optimized if the left hand side is an empty domain
 ***************************************************************/
proc spDomDistAssign31(spDom1, spDom2)
{

 if(!spAssignCheck(spDom1, spDom2) )
  {
    //warning(" NO-OP: CA-Assignment of block-distributed sparse domains is not allowed"); 
     return; 
  }

  spDomClear(spDom1);
  if(spDom2.size == 0) then
    return;

  var locDoms1 =  spDom1._value.locDoms;
  var locDoms2 =  spDom2._value.locDoms;

  //spDom1.distM.targetLocales is equivalent to spDom1.targetLocales()
  coforall l in  spDom1._value.dist.targetLocDom do on spDom1._value.dist.targetLocales[l]   
  {
    var locDom1 = locDoms1[l]; // this requires access to non-distributed locDoms1 array 
    var locDom2 = locDoms2[l];
      local        
      {
        locDom1.mySparseBlock +=  locDom2.mySparseBlock;  
      }
 }

   spDom1._value.nnz = getGlobalNnz(spDom1);//spDom2._value.nnz;

}
 

proc spDomDistAssign(spDom1, spDom2)
{
  if(spDom2.size == 0) then
    spDom1.clear();
  else
    spDomDistAssign3(spDom1, spDom2);
}



proc spArrDistAssign1(x: [?xDom], y: [?yDom])
{
    var t1, t2 : Timer;
   t1.start();

  spDomDistAssign2(xDom, yDom);
  if(xDom.size == 0) then
    return;
   t1.stop();
   t2.start();
  
   forall i in xDom do
     x[i] = y[i];
 
  t2.stop();
   //writeln(" [1] domain assign: ", t1.elapsed(), "  array assign: ",  t2.elapsed());

}


proc spArrDistAssign2(x: [?xDom], y: [?yDom])
{
    var t1, t2 : Timer;
   t1.start();

  spDomDistAssign3(xDom, yDom);
  if(xDom.size == 0) then
    return;
   t1.stop();
   t2.start();

  var locArrs1 =  x._value.locArr;
  var locArrs2 =  y._value.locArr;    
  coforall (locArr1, locArr2) in zip(locArrs1, locArrs2) do 
  on locArr1 do
  {
     var lo = locArr1.myElems.domain._value.nnzDom.low; // usually 1
     forall i in lo..#locArr1.myElems.size do
          locArr1.myElems._value.data[i] = locArr1.myElems._value.data[i]; 
    /*
           forall (a,b) in zip(locArr1.myElems._value.data, locArr2.myElems._value.data) do
             a = b;
     */
  }

  t2.stop();
  // writeln("[2] domain assign: ", t1.elapsed(), "  array assign: ",  t2.elapsed());

}


// currently it serves the purpose of removing visited vertices 
//TODO: need to make it general eWiseMult / setminus

  proc eWiseMult(x: [?vecDom], visited, numTasks)
  {

    var lo = vecDom.low; 
    var hi = vecDom.high;
    const denseDom = {lo..hi} dmapped Block({lo..hi});
    var newDom: sparse subdomain(denseDom);
    var newArr: [newDom] x.eltType;

    var oldDoms =  vecDom._value.locDoms;
    var newDoms =  newDom._value.locDoms;
    var oldArrs =  x._value.locArr;
    var newArrs =  newArr._value.locArr;
    var dArrs = visited._value.locArr;
    //const numTasks = 1; // check how to determine this 
    coforall (oldDom,newDom,oldArr, newArr, dArr) in zip(oldDoms, newDoms, oldArrs, newArrs, dArrs) do
      on oldDom 
      {

        var oldnnz = oldDom.mySparseBlock.size;
        var perthread = (oldnnz / numTasks) : int;
        var keepInd : [0..#oldnnz] oldDom.idxType;
        var keepVal : [0..#oldnnz] oldArr.eltType;
        var lo = oldArr.myElems.domain._value.nnzDom.low; // usually 1
        var tStartInds : [0..#numTasks] int;
        var tEndInds : [0..#numTasks] int;
        var tDisp : [0..numTasks] int; // size = numTasks+1 

        coforall tid in 0..#numTasks // shared memory parallel
        {
          var tStartInd = perthread * tid;
          var tEndInd = perthread * (tid+1);
          if(tid == numTasks) then
            tEndInd = oldnnz; 
          var k = tStartInd;
          for i in  tStartInd..(tEndInd-1)
          {
            var spInd =  oldDom.mySparseBlock._value.indices[i+lo];  
            var spVal = oldArr.myElems._value.data[i+lo]; 
            if(!dArr[spInd])
            {
                keepInd[k] = spInd;
                keepVal[k] = spVal;
                k += 1;
                dArr[spInd] = true;
            }  
          }
          tStartInds[tid] = tStartInd;
          tEndInds[tid] = k;
        } 

        // compute displacement 
        tDisp[0] = 0;
        for i in 0..#numTasks
        {
          tDisp[i+1] = tDisp[i] +  tEndInds[i] -  tStartInds[i];
        }

        var newLocalNnz = tDisp[numTasks];

        //newDom.mySparseBlock._value._bulkGrow(newLocalNnz);
        newDom.mySparseBlock._value.nnz = newLocalNnz;
        newDom.mySparseBlock._value.nnzDom = {1..newLocalNnz};

        coforall tid in 0..#numTasks // shared memory parallel
        {
          var tStartInd1 = tStartInds[tid];
          var tEndInd1 =  tEndInds[tid];
          var tStartInd2 = tDisp[tid];
          var tEndInd2 = tDisp[tid+1];

          for (i,j) in  zip(tStartInd1..(tEndInd1-1), tStartInd2..(tEndInd2-1))
          {
            newDom.mySparseBlock._value.indices[j+lo] = keepInd[i];  
            newArr.myElems._value.data[j+lo] = keepVal[i]; 
          }
        }  
  }
  newDom._value.nnz = getGlobalNnz(newDom);
  return newArr;

  }




proc testAssignDist()
{
   
  writeln("****** distributed sparse domain assignment *********");

  var dDomDist1 = {0..9} dmapped Block({0..9});  
  var dDomDist2 = {0..9} dmapped Block({0..9});
  // both parentdomain and bounding box must match for communication avoiding assignment

  var spDomDist1: sparse subdomain(dDomDist1);
  var spDomDist2: sparse subdomain(dDomDist2);
  spDomDist1 = (0,5,7); // this is sequential 
  spDomDist2 = (1,2,6,7); 
  var spArrDist1: [spDomDist1] int;
  var spArrDist2: [spDomDist2] int;
   writeln(dDomDist1.dims());
   writeln(spDomDist1.dims());
     //writeln(spDomDist1.dims());




  
  writeln("--------- before assignment --------");
  writeln("spDomDist1: ", spDomDist1);
  writeln("spArrDist1: ", spArrDist1);
  writeln("spDomDist2: ", spDomDist2);
  writeln("spArrDist2: ", spArrDist2);
  writeln("------------------------------------");
  //spDomDistAssign1(spDomDist1, spDomDist2);
  spDomDistAssign3(spDomDist1, spDomDist2);

  writeln("--------- after assignment --------");
  writeln("spDomDist1: ", spDomDist1);
  writeln("spArrDist1: ", spArrDist1);
  writeln("spDomDist2: ", spDomDist2);
  writeln("spArrDist2: ", spArrDist2);
  writeln("------------------------------------");

    getGlobalNnz(  spDomDist1 );


}


}

//testAssignDist();




/*


/* functionalities added by Ariful Azad: LBNL */


proc SparseBlockArr.dsiHasSingleLocalSubdomain() param return true;
proc SparseBlockDom.dsiHasSingleLocalSubdomain() param return true;
proc SparseBlockArr.dsiLocalSubdomain() return  myLocArr.locDom.mySparseBlock;
proc BlockDom.dsiLocalSubdomain() {
  // TODO -- could be replaced by a privatized myLocDom in BlockDom
  // as it is with BlockArr
  var myLocDom:LocBlockDom(rank, idxType, stridable) = nil;
  for (loc, locDom) in zip(dist.targetLocales, locDoms) {
    if loc == here then
      myLocDom = locDom;
  }
  return myLocDom.myBlock;
}


*/
