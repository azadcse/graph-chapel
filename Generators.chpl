module Generators
{

use Time;
use Random;
use Sort;


/*
A simplified Erdos-Renyi generator,
Gnerated an nxn matrix with d nonzeros per column
Inputs:
  n: number of rows/columns of the matrix
  d: number of nonzeros per row/column
 */
  proc ER(scale, d) 
  {
    writeln("\nGenerating Erdos-Renyi random matrix");
    var n = 2**scale;
    writeln("scale=", scale, " d=", " nnz=", n*d);
    var t: Timer;
    var rsInd = new RandomStream();
    var rsVal = new RandomStream();  
    var nnz = n*d;
    var inds: [{0..#nnz}] 2*int;
    //var rinds : [0..nnz-1] int;
    //var cinds : [0..nnz-1] int;
    var vals : [0..#nnz] real;

    // Populate the sparse domain 
    var k = 0;
    t.start();
    for i in 0..n-1  // for each row .. test for each column as well
    {
      for j in 0..d-1 
      {     
        var cind = (rsInd.getNext() * (n-1)) : int ; 
        inds[k] = (i,cind);
        vals[k] = rsVal.getNext();
        k = k+1;
      }    
    }

    t.stop();
    writeln("ER Generator took: ", t.elapsed()," seconds");
    return (n, n, inds, vals);
  }


  proc RandVec(n,nz) 
  {
    var rs = new RandomStream(); 
    var idx : [0..nz-1] int;
    var t: Timer;
    t.start();

    forall i in 0..nz-1  
    {
      // get a random index 
        idx[i] =  (rs.getNext() * (n-1)) : int ; 
    }
 
    var vecDom: sparse subdomain({0..n-1});
    vecDom += idx;
    var x: [vecDom] int;   
    forall i in x.domain do
      x[i] = (1 + rs.getNext() * 9) : int ;
    t.stop();
    writeln("Random vector generator took: ", t.elapsed()," seconds");

    return x;
}


}
