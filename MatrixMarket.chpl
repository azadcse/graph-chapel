module MatrixMarket {

   use FileSystem;
   use Math;
   use IO;
   use Sys;
   use List;
   use Time;

   class MMWriter {
      var HEADER_LINE = "%%MatrixMarket matrix coordinate real general\n"; // currently the only supported MM format in this module

      var fd:file;
      var fout:channel(true, iokind.dynamic, true);

      var headers_written:bool;
      var last_rowno:int;

      proc MMWriter(const fname:string) {
         fd = open(fname, iomode.cw, iokind.native);
         fout = fd.writer(start=0);
         headers_written=false;
      }

      proc write_headers(nrows, ncols, nnz=-1) {
         fout.write(HEADER_LINE);
         fout.write("%\n");

         if nnz < 0 {
            var blank = "                                                  \n";
            fout.write(blank);
         }
         else {
            fout.writef("%i %i %i\n", nrows, ncols, nnz);
         }

         last_rowno=-1;
         headers_written=true;
      }

      proc fake_headers(nrows, ncols, nnz) {
         var tfout = fd.writer(start=HEADER_LINE.size);
         tfout.writef("%i %i %i", nrows, ncols, nnz);
         tfout.close();
      }

      proc write_vector(i:int, jvec:[?Djvec] ?T) where Djvec.rank == 1 {
         assert(last_rowno < i, "rows %i and %i not in sequential order!", last_rowno, i);
         for (j,w) in zip(Djvec, jvec) {
            if abs(w) > 1e-12 { fout.writef("%i %i %s\n", i, j, w); }
         }

         last_rowno = i;
         var ret:(int,int);
         if jvec.size < 1 { ret = (-1, 0); } else { ret = (Djvec.size, jvec.size); }
         return ret;
      }

      proc close() { fout.close(); fd.close(); }
      proc ~MMWriter() { this.close(); }
   }

proc mmwrite(const fname:string, mat:[?Dmat] real, _num_cols=-1) where mat.domain.rank == 2 {
   var mw = new MMWriter(fname);
   mw.write_headers(-1,-1,-1);
   var (ncols, nnz) = (0,0);
   var (nrows, poslast) = (-1,-1);
   var n_cols = _num_cols;
   for r in 1..Dmat.high(1) {
      var row = mat(r,..);
      var (max_id, veclen) = mw.write_vector(r, row);
      n_cols = max(n_cols, max_id);
      nnz += veclen;
      ncols = r;
   }

   nrows = mw.last_rowno;
   ncols = n_cols; //if ncols != 0 then ncols else n_cols;

   mw.fake_headers(nrows, ncols, nnz);
   mw.close();
   delete mw;
}

class MMReader {
   var fd:file;
   var fin:channel(false, iokind.dynamic, true);
   var isSymmetric:bool;
   var isPattern:bool;
   var isReal:bool;
   var isInteger:bool;

   proc MMReader(const fname:string) {
      fd = open(fname, iomode.r, hints=IOHINT_SEQUENTIAL|IOHINT_CACHED);
      fin = fd.reader(start=0, hints=IOHINT_SEQUENTIAL|IOHINT_CACHED);
   }

   proc read_header() {
      var header:string;
      assert(fin.readline(header) == true, "MMReader I/O error!");
     // assert(header == "%%MatrixMarket matrix coordinate real general\n", "attempted to load an unsupported file");

      if(header.find("symmetric")!=0) {isSymmetric = true;}
      else {isSymmetric = false;}

      if(header.find("pattern")!=0) {isPattern = true;}
      else {isPattern = false;}

      if(header.find("integer")!=0) {isInteger = true;}
      else {isInteger = false;}

      if(header.find("real")!=0) {isReal = true;}
      else {isReal = false;}

      if(header.find("complex")!=0) {writeln("Sorry, this application does not support complex types");}
 




     // check for comments starting with "%" beneath the matrix market format header
      var percentfound:string;
      var offset = fin._offset();
      fin.readline(percentfound);
      while(percentfound.startsWith("%"))
      {
        offset = fin._offset();
        fin.readline(percentfound);
      }

      // rewind channel by length of the last-read read string
      fin = fd.reader(start=offset, hints=IOHINT_SEQUENTIAL|IOHINT_CACHED);
   }

   proc read_matrix_info() {
      var nrows, ncols, nnz:int;
      var done = fin.readf("%i %i %i", nrows, ncols, nnz);
      assert(done == true, "error reading matrix market file's information");
      if isSymmetric then 
        nnz = nnz * 2; // just an upper bound, need to be adjasted later
      return (nrows, ncols, nnz);
   }

   iter read_data() {
      var done:bool = true;
      while done {
         var i, j:int;
         var w:real;
         if(isPattern)
         {
            done = fin.readf("%i %i\n", i, j);
            w = 1.0; // keep some default value
         }
         else
         {
           if(isInteger)
           {
              var wi:int;
              done = fin.readf("%i %i %i\n", i, j, wi);
              w = wi:real; // convert to real 
           }
           else
           {
              done = fin.readf("%i %i %r\n", i, j, w);
           }
                     
         }
         if done { yield (i,j,w); }
      }
   }

   proc read_file() {
      read_header();
      var (nrows, ncols, nnz) = read_matrix_info();
      writeln("\nReading a matrix market file");
      writeln("nrows : ", nrows, " ncol : ", ncols, " nnz : ", nnz);
      stdout.flush();
     
      var inds: [{0..#nnz}] 2*int;
      var vals : [0..#nnz] real;
      //var Dtoret = {1..nrows, 1..ncols}; // maybe look for sparse matrix support here?
      //var toret:[Dtoret] real;
      var k = 0;
      var percent = 0;
      // TODO: parallelizing this read would be nice...
      for (i,j,w) in read_data() 
      {
        inds[k] = (i-1, j-1); // we use zero-based indexing 
        vals[k] = w;
        k = k + 1;
        if(isSymmetric && i!=j)
        {
          inds[k] = (j-1, i-1);
          vals[k] = w;
          k = k + 1;
        }
        var percentDone = (k:real/nnz*100):int;
        if(percentDone==percent) 
        {
          write(percentDone, "%..");
          stdout.flush();
          percent = percent + 5;
        }
      }
      if (isSymmetric)
        {
          // remove the extra memory at the end
          inds.remove(k, nnz-k);
          vals.remove(k, nnz-k);
        }

      return (nrows, ncols, inds, vals);
   }

   proc close() { 
      fin.close(); 
      fd.close(); 
   }

   proc ~MMReader() { this.close(); }
}

proc mmread(const fname:string) {
   var t: Timer;
   t.start();
   var mr = new MMReader(fname);
   var toret = mr.read_file();
   t.stop();
   writeln("\nFinished reading matrix market file in ", t.elapsed()," seconds.\n");
   stdout.flush();


   delete mr;
   return toret;
}

}

