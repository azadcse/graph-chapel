bfs:  Main.chpl SpMSpV.chpl BFS.chpl MatrixMarket.chpl Generators.chpl BuildSpMat.chpl spAssign.chpl 
	chpl Main.chpl SpMSpV.chpl BFS.chpl MatrixMarket.chpl Generators.chpl BuildSpMat.chpl spAssign.chpl --fast -o bfs

test:	test.chpl spAssign.chpl 
	chpl test.chpl spAssign.chpl --fast -o test

applytest:	test.chpl  
	chpl test.chpl spAssign.chpl  --fast -o applytest


assigntest:	test.chpl spAssign.chpl 
	chpl test.chpl spAssign.chpl --fast -o assigntest

emultest:	test.chpl spAssign.chpl 
	chpl test.chpl spAssign.chpl --fast -o emultest

spmspvtest:	spmspvtest.chpl SpMSpV.chpl MatrixMarket.chpl  BuildSpMat.chpl spAssign.chpl 
	chpl spmspvtest.chpl SpMSpV.chpl MatrixMarket.chpl  BuildSpMat.chpl spAssign.chpl --fast -o spmspv

