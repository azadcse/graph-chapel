module Utility
{

  use Random;
  use BlockDist;
  use SpAssign;
  proc PrintCSR(A: [?CSRDom]) 
  {

    writeln(CSRDom.dims());
  for i in  CSRDom.dim(1)
  {
    var rstart = CSRDom._value.rowStart(i);
    var rend = CSRDom._value.rowStop(i);
    var nzidx = rstart; 
    var colid = CSRDom._value.colIdx(nzidx);
    for j in CSRDom.dim(2)
    {
      if(j==colid && rstart<=rend)
      {
         write(A._value.data(nzidx), ' ');
         nzidx += 1;
         if(nzidx<rend) then
          colid = CSRDom._value.colIdx(nzidx);
      }
      else
      {
         write(". ");
      }
    }
    writeln();
  }

}

  // print sequentially 
  proc  PrintDistSparse(A: [?MatDom]) 
  {
    var distM = MatDom._value.dist;
    for l in  distM.targetLocDom do// on distM.targetLocales[l]  
    {
      writeln("Submatrix in Grid : ", l);
      var locArr =  A._value.locArr[l].myElems; // equvalent to: A._value.myLocArr.myElems;

      writeln(locArr.domain); 
      writeln();  
    } 
  
  }

 // print sequentially 
  proc  PrintDistCSR(A: [?MatDom]) 
  {
    var distM = MatDom._value.dist;
    for l in  distM.targetLocDom do// on distM.targetLocales[l]  
    {
      writeln("Submatrix in Grid : ", l);
      var locArr =  A._value.locArr[l].myElems; // equvalent to: A._value.myLocArr.myElems;

      PrintCSR(locArr); 
      writeln();  
    } 
  
  }



  proc WriteSpMat(A, n)
  {
    for i in 0..n-1 do
      for j in 0..n-1 
      {
        write(A[i,j]);
        if (j == n-1) then
          writeln();
        else
          write(" ");
      }
    writeln();
  }

  proc assign(x: [?xDom], y: [?yDom]) 
  {
    xDom.clear();
    if(y.size>0) // without this check the code still works
    {
      var denseY : [0..#y.size] y.eltType;
      for (ind, k) in zip(yDom, 0..)
      {
        denseY[k] = ind;
      }
      xDom.bulkAdd(denseY,true, true, false); 
      for ind in xDom
      {
        x[ind] = y[ind];
      }
    }
  }


 proc assignDist(x: [?xDom], y: [?yDom]) 
  {
    writeln("before size: ", xDom, " ", x.size, " ", x);
    xDom.clear(); 
    //xDom = yDom;
    writeln("after size: ",  xDom, " ", x.size, " ", x);
    
/*
    if(y.size>0) // without this check the code still works
    {
      var denseY : [0..#y.size] y.eltType;
      for (ind, k) in zip(yDom, 0..)
      {
        denseY[k] = ind;
      }
      xDom.bulkAdd(denseY,true, true, false); 
      for ind in xDom
      {
        x[ind] = y[ind];
      }
    }
 */
  }



// Create a bock-distributed sparse vector 
// with nonzero at nnz random locations
proc createSpVecRandFast(size, nnz)
{

  var dDom = {0..#size} dmapped Block({0..#size});
  var spDom: sparse subdomain(dDom);
  var spArr: [spDom] int;


  var locDoms =  spDom._value.locDoms;  
  var locArrs =  spArr._value.locArr;
   
  coforall (locDom, locArr) in zip(locDoms, locArrs) do 
  on locDom do
  {
      var locNnz = nnz/numLocales: int; // currently make it balanced
      locDom.mySparseBlock._value.nnz = locNnz;
      if(locNnz > 0)
      {
        locDom.mySparseBlock._value.nnzDom = {1..locNnz};
        var globalIndLow =  locDom.mySparseBlock.low;
        var parentDomSize = locDom.mySparseBlock.dim(1).size;
        var gap = parentDomSize/locNnz: int;
        forall i in 1..#locNnz
        {
          locDom.mySparseBlock._value.indices[i] = globalIndLow + (i-1)*gap; 
          locArr.myElems._value.data[i] =  globalIndLow + (i-1)*gap;
        }
      }
  }
  spDom._value.nnz = getGlobalNnz(spDom);
  return spArr;
}



// Create a bock-distributed sparse vector 
// with nonzero at nnz random locations
proc initDenseVecFast(A, nnz, val)
{
  var locArrs =  A._value.locArr;
   
  coforall locArr in locArrs do 
  on locArr do
  {
      var locNnz = nnz/numLocales: int; 
      if(locNnz > 0)
      {
        var gap = locArr.myElems.size/locNnz: int;
        forall i in locArr.myElems.domain.low..locArr.myElems.domain.high by gap        
        {
          locArr[i] =  val;
        }
      }
  }
}



// Create a bock-distributed sparse vector 
// with nonzero at nnz random locations
proc createSpVecRand(size, nnz)
{
  //var rs = makeRandomStream(); 
  var arr: [0..#nnz] int; 
  // fillRandom(arr); // does not work in my laptop
  var gap = size/nnz: int;
  forall i in 0..#nnz
  {
    arr[i] = i*gap ;  
  }
   var dDomDist = {0..#size} dmapped Block({0..#size});
  var spDomDist: sparse subdomain(dDomDist);
  spDomDist.bulkAdd(arr, true, true, false);


  var spArrDist: [spDomDist] int;
  /*
  forall arr in spArrDist
  {
    on arr
    {
      arr = here.id;
    }
  }*/
  //writeln("Created a bock-distributed sparse vector with ");
  //writeln(" size: ", spArrDist.domain.dims(), " nnz: ", spArrDist.size);

  return spArrDist;
}



}
